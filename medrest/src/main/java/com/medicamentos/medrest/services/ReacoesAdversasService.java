package com.medicamentos.medrest.services;

import com.medicamentos.medrest.dtos.ReacoesAdversasDTO;
import com.medicamentos.medrest.models.MedicamentoModel;
import com.medicamentos.medrest.models.ReacoesAdversasModel;
import com.medicamentos.medrest.repositories.MedicamentoRepository;
import com.medicamentos.medrest.repositories.ReacoesAdversasRepository;
import java.util.Optional;
import java.util.UUID;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ReacoesAdversasService {

  @Autowired
  MedicamentoRepository medicamentoRepository;
  @Autowired
  ReacoesAdversasRepository reacoesAdversasRepository;

  @Transactional
  public Object save(UUID id, ReacoesAdversasDTO reacoesAdversaDto) {
    MedicamentoModel medicamento = medicamentoRepository.findById(id)
        .orElseThrow();

    var reacoesAdversasModel = new ReacoesAdversasModel();
    reacoesAdversasModel.setDescricao(reacoesAdversaDto.getDescricao());
    reacoesAdversasModel.setMedicamentoId(medicamento);

    medicamento.addReacoesAdversas(reacoesAdversasModel);
    return medicamentoRepository.save(medicamento);
  }

  public Page<ReacoesAdversasModel> findAll(Pageable pageable) {
    return reacoesAdversasRepository.findAll(pageable);
  }


  public Optional<ReacoesAdversasModel> findById(UUID id) {
    return reacoesAdversasRepository.findById(id);
  }

  @Transactional
  public void delete(ReacoesAdversasModel reacoesAdversasModel) {
    reacoesAdversasRepository.delete(reacoesAdversasModel);
  }

  @Transactional
  public MedicamentoModel update(ReacoesAdversasDTO reacaoAdversasDTO,
      UUID id) {
    MedicamentoModel medicamento = medicamentoRepository.findById(
        reacaoAdversasDTO.getMedicamentoID()).orElseThrow();

    var reacoesAdversasModel = new ReacoesAdversasModel();

    reacoesAdversasModel.setDescricao(reacaoAdversasDTO.getDescricao());
    reacoesAdversasModel.setId(id);
    reacoesAdversasModel.setMedicamentoId(medicamento);

    medicamento.addReacoesAdversas(reacoesAdversasModel);
    return medicamentoRepository.save(medicamento);
  }
}
