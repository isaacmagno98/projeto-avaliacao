package com.medicamentos.medrest.services;

import com.medicamentos.medrest.dtos.MedicamentoDTO;
import com.medicamentos.medrest.models.MedicamentoModel;
import com.medicamentos.medrest.repositories.FabricanteRepository;
import com.medicamentos.medrest.repositories.MedicamentoRepository;
import java.util.Optional;
import java.util.UUID;
import javax.transaction.Transactional;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class MedicamentoService {

  @Autowired
  MedicamentoRepository medicamentoRepository;

  @Autowired
  FabricanteRepository fabricanteRepository;

  @Transactional
  public MedicamentoModel save(MedicamentoDTO medicamentoDto) {
    var medicamentoModel = new MedicamentoModel();
    var fabricanteModel = fabricanteRepository.findById(
        medicamentoDto.getFabricanteId()).orElseThrow();

    BeanUtils.copyProperties(medicamentoDto, medicamentoModel);
    medicamentoModel.setFabricanteId(fabricanteModel);

    return medicamentoRepository.save(medicamentoModel);
  }

  public Page<MedicamentoModel> findAll(Pageable pageable) {
    return medicamentoRepository.findAll(pageable);
  }

  public Optional<MedicamentoModel> findById(UUID id) {
    return medicamentoRepository.findById(id);
  }

  @Transactional
  public void delete(MedicamentoModel medicamentoModel) {
    medicamentoRepository.delete(medicamentoModel);
  }

  @Transactional
  public MedicamentoModel update(MedicamentoDTO medicamentoDto, UUID id) {
    var medicamentoModel = new MedicamentoModel();
    var fabricanteModel = fabricanteRepository.findById(
        medicamentoDto.getFabricanteId()).orElseThrow();

    BeanUtils.copyProperties(medicamentoDto, medicamentoModel);
    medicamentoModel.setId(id);
    medicamentoModel.setFabricanteId(fabricanteModel);

    return medicamentoRepository.save(medicamentoModel);
  }
}
