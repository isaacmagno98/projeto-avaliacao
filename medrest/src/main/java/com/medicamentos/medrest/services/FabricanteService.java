package com.medicamentos.medrest.services;

import com.medicamentos.medrest.dtos.FabricanteDTO;
import com.medicamentos.medrest.models.FabricanteModel;
import com.medicamentos.medrest.repositories.FabricanteRepository;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class FabricanteService {

  @Autowired
  FabricanteRepository fabricanteRepository;

  @Transactional
  public FabricanteModel save(FabricanteDTO fabricanteDto) {
    var fabricanteModel = new FabricanteModel();

    fabricanteModel.setNome(fabricanteDto.getNome());

    return fabricanteRepository.save(fabricanteModel);
  }

  public Page<FabricanteModel> findAll(Pageable pageable) {

    return fabricanteRepository.findAll(pageable);
  }

}
