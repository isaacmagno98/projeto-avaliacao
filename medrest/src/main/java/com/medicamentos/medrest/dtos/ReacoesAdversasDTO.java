package com.medicamentos.medrest.dtos;

import java.util.UUID;

public class ReacoesAdversasDTO {

  private String descricao;

  private UUID medicamentoID;

  public String getDescricao() {
    return descricao;
  }

  public void setDescricao(String descricao) {
    this.descricao = descricao;
  }


  public UUID getMedicamentoID() {
    return medicamentoID;
  }

  public void setMedicamentoID(UUID medicamentoID) {
    this.medicamentoID = medicamentoID;
  }
}
