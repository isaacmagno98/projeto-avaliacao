package com.medicamentos.medrest.dtos;

import com.medicamentos.medrest.models.ReacoesAdversasModel;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public class MedicamentoDTO {

  private String nome;
  private String anvisa;
  private LocalDate dataValidade;
  private String telefoneSac;
  private Double preco;
  private Integer quantidade;
  private List<ReacoesAdversasModel> reacoesAdversas;

  private UUID fabricanteId;


  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getAnvisa() {
    return anvisa;
  }

  public void setAnvisa(String anvisa) {
    this.anvisa = anvisa;
  }

  public LocalDate getDataValidade() {
    return dataValidade;
  }

  public void setDataValidade(LocalDate dataValidade) {
    this.dataValidade = dataValidade;
  }

  public String getTelefoneSac() {
    return telefoneSac;
  }

  public void setTelefoneSac(String telefoneSac) {
    this.telefoneSac = telefoneSac;
  }

  public Double getPreco() {
    return preco;
  }

  public void setPreco(Double preco) {
    this.preco = preco;
  }

  public Integer getQuantidade() {
    return quantidade;
  }

  public void setQuantidade(Integer quantidade) {
    this.quantidade = quantidade;
  }


  public List<ReacoesAdversasModel> getReacoesAdversas() {
    return reacoesAdversas;
  }

  public void setReacoesAdversas(List<ReacoesAdversasModel> reacoesAdversas) {
    this.reacoesAdversas = reacoesAdversas;
  }

  public UUID getFabricanteId() {
    return fabricanteId;
  }

  public void setFabricanteId(UUID fabricanteId) {
    this.fabricanteId = fabricanteId;
  }
}
