package com.medicamentos.medrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MedrestApplication {

	public static void main(String[] args) {
		SpringApplication.run(MedrestApplication.class, args);
	}

}
