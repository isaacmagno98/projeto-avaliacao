package com.medicamentos.medrest.controllers;

import com.medicamentos.medrest.dtos.FabricanteDTO;
import com.medicamentos.medrest.models.FabricanteModel;
import com.medicamentos.medrest.services.FabricanteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/fabricantes")
public class FabricanteController {

  @Autowired
  FabricanteService fabricanteService;


  @PostMapping
  public ResponseEntity<Object> salvarFabricante(
      @RequestBody FabricanteDTO fabricanteDto) {

    return ResponseEntity.status(HttpStatus.CREATED)
        .body(fabricanteService.save(fabricanteDto));
  }

  @GetMapping
  public ResponseEntity<Page<FabricanteModel>> listarFabricantes(
      @PageableDefault(page = 0, size = 10, sort = "id", direction =
          Sort.Direction.ASC) Pageable pageable) {

    return ResponseEntity.status(HttpStatus.OK)
        .body(fabricanteService.findAll(pageable));
  }

}
