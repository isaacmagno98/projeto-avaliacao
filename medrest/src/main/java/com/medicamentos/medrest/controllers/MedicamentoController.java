package com.medicamentos.medrest.controllers;

import com.medicamentos.medrest.dtos.MedicamentoDTO;
import com.medicamentos.medrest.models.MedicamentoModel;
import com.medicamentos.medrest.services.MedicamentoService;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/medicamentos")
public class MedicamentoController {

  @Autowired
  MedicamentoService medicamentoService;


  @PostMapping
  public ResponseEntity<Object> salvarMedicamento(
      @RequestBody MedicamentoDTO medicamentoDto) {

    return ResponseEntity.status(HttpStatus.CREATED)
        .body(medicamentoService.save(medicamentoDto));
  }

  @GetMapping
  public ResponseEntity<Page<MedicamentoModel>> listarMedicamentos(
      @PageableDefault(page = 0, size = 10, sort = "id", direction =
          Sort.Direction.ASC) Pageable pageable) {
    return ResponseEntity.status(HttpStatus.OK)
        .body(medicamentoService.findAll(pageable));
  }

  @GetMapping("/{id}")
  public ResponseEntity<Object> listarMedicamento(
      @PathVariable(value = "id") UUID id) {

    var medicamento = medicamentoService.findById(id);

    if (!medicamento.isPresent()) {
      return ResponseEntity.status(HttpStatus.NOT_FOUND)
          .body("Medicamento não encontrado!");
    }

    return ResponseEntity.status(HttpStatus.OK).body(medicamento);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<Object> deletarMedicamento(
      @PathVariable(value = "id") UUID id) {

    var medicamento = medicamentoService.findById(id);

    if (!medicamento.isPresent()) {
      return ResponseEntity.status(HttpStatus.NOT_FOUND)
          .body("Medicamento não encontrado!");
    }

    medicamentoService.delete(medicamento.get());

    return ResponseEntity.status(HttpStatus.OK)
        .body("Medicamento deletado com sucesso!");
  }

  @PutMapping("/{id}")
  public ResponseEntity<Object> atualizarMedicamento(
      @PathVariable(value = "id") UUID id,
      @RequestBody MedicamentoDTO medicamentoDto) {
    var medicamento = medicamentoService.findById(id);

    if (!medicamento.isPresent()) {
      return ResponseEntity.status(HttpStatus.NOT_FOUND)
          .body("Medicamento não encontrado!");
    }

    return ResponseEntity.status(HttpStatus.OK)
        .body(medicamentoService.update(medicamentoDto, id));
  }
}
