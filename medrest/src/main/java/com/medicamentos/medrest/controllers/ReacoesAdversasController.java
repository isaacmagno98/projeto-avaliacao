package com.medicamentos.medrest.controllers;

import com.medicamentos.medrest.dtos.ReacoesAdversasDTO;
import com.medicamentos.medrest.models.ReacoesAdversasModel;
import com.medicamentos.medrest.services.ReacoesAdversasService;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/reacoes-adversas")
public class ReacoesAdversasController {

  @Autowired
  ReacoesAdversasService reacoesAdversasService;

  @PostMapping("/medicamento/{id}")
  public ResponseEntity<Object> criarReacaoAdversa(@PathVariable UUID id,
      @RequestBody ReacoesAdversasDTO reacoesAdversaDto) {
    return ResponseEntity.status(HttpStatus.CREATED)
        .body(reacoesAdversasService.save(id, reacoesAdversaDto));
  }

  @GetMapping
  public ResponseEntity<Page<ReacoesAdversasModel>> listarReacoesAdversas(
      @PageableDefault(page = 0, size = 10, sort = "id", direction =
          Sort.Direction.ASC) Pageable pageable) {
    return ResponseEntity.status(HttpStatus.OK)
        .body(reacoesAdversasService.findAll(pageable));
  }

  @GetMapping("/{id}")
  public ResponseEntity<Object> listarReacaoAdversa(
      @PathVariable(value = "id") UUID id) {

    var reacaoAdversa = reacoesAdversasService.findById(id);

    if (!reacaoAdversa.isPresent()) {
      return ResponseEntity.status(HttpStatus.NOT_FOUND)
          .body("Reação Adversa não encontrada");
    }

    return ResponseEntity.status(HttpStatus.OK).body(reacaoAdversa);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<Object> deletarReacaoAdversa(
      @PathVariable(value = "id") UUID id) {
    var reacaoAdversa = reacoesAdversasService.findById(id);

    if (!reacaoAdversa.isPresent()) {
      return ResponseEntity.status(HttpStatus.NOT_FOUND)
          .body("Reação Adversa não encontrada");
    }

    reacoesAdversasService.delete(reacaoAdversa.get());

    return ResponseEntity.status(HttpStatus.OK)
        .body("Reação Adversa deletada com sucesso!");
  }

  @PutMapping("/{id}")
  public ResponseEntity<Object> atualizarReacaoAdversa(
      @PathVariable(value = "id") UUID id,
      @RequestBody ReacoesAdversasDTO reacaoAdversasDTO) {
    var reacaoAdversa = reacoesAdversasService.findById(id);

    if (!reacaoAdversa.isPresent()) {
      return ResponseEntity.status(HttpStatus.NOT_FOUND)
          .body("Reação Adversa não encontrada");
    }

    return ResponseEntity.status(HttpStatus.OK)
        .body(reacoesAdversasService.update(reacaoAdversasDTO, id));
  }
}
