package com.medicamentos.medrest.config;

public enum FacricantesEnum {

  MERCK("MERCK"), TAKEDA("TAKEDA"), EUROFARMA("EUROFARMA"), MEDLEY(
      "MEDLEY"), GLAXOSMITHKLINE("GLAXOSMITHKLINE"), BAYER("BAYER"), ROCHE(
      "ROCHE"), ELILILLY("ELILILLY"), BRAINFARMA("BRAINFARMA"), PFIZER(
      "PFIZER");

  private String fabricante;

  FacricantesEnum(String fabricante) {
    this.fabricante = fabricante;
  }

  public String getFabricante() {
    return fabricante;
  }
}
