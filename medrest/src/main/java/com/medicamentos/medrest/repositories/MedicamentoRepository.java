package com.medicamentos.medrest.repositories;

import com.medicamentos.medrest.models.MedicamentoModel;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicamentoRepository extends
    JpaRepository<MedicamentoModel, UUID> {

  Optional<MedicamentoModel> findById(UUID id);
}
