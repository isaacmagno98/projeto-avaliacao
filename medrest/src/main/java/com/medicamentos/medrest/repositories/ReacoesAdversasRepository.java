package com.medicamentos.medrest.repositories;

import com.medicamentos.medrest.models.ReacoesAdversasModel;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReacoesAdversasRepository extends
    JpaRepository<ReacoesAdversasModel, UUID> {

  Optional<ReacoesAdversasModel> findById(UUID id);

}
