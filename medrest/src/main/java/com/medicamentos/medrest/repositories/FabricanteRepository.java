package com.medicamentos.medrest.repositories;

import com.medicamentos.medrest.models.FabricanteModel;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FabricanteRepository extends
    JpaRepository<FabricanteModel, UUID> {

  Optional<FabricanteModel> findById(UUID id);

}
