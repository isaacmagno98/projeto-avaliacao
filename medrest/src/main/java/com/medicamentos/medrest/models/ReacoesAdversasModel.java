package com.medicamentos.medrest.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TB_REACOES_ADVERSAS")
public class ReacoesAdversasModel implements Serializable {

  @ManyToOne
  @JoinColumn(name = "medicamento_id")
  @JsonIgnore
  MedicamentoModel medicamentoId;
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID id;
  private String descricao;

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getDescricao() {
    return descricao;
  }

  public void setDescricao(String descricao) {
    this.descricao = descricao;
  }

  public MedicamentoModel getMedicamentoId() {
    return medicamentoId;
  }

  public void setMedicamentoId(MedicamentoModel medicamentoId) {
    this.medicamentoId = medicamentoId;
  }
}
