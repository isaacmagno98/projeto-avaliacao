package com.medicamentos.medrest.models;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "TB_MEDICAMENTO")
public class MedicamentoModel implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID id;

  private String nome;
  private String anvisa;
  private LocalDate dataValidade;
  private String telefoneSac;
  private Double preco;
  private Integer quantidade;

  @OneToMany(mappedBy = "medicamentoId", cascade = CascadeType.ALL)
  private List<ReacoesAdversasModel> reacoesAdversas =
      new ArrayList<ReacoesAdversasModel>();

  @ManyToOne
  @JoinColumn(name = "fabricante_id")
  private FabricanteModel fabricanteId;

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getAnvisa() {
    return anvisa;
  }

  public void setAnvisa(String anvisa) {
    this.anvisa = anvisa;
  }

  public LocalDate getDataValidade() {
    return dataValidade;
  }

  public void setDataValidade(LocalDate dataValidade) {
    this.dataValidade = dataValidade;
  }

  public String getTelefoneSac() {
    return telefoneSac;
  }

  public void setTelefoneSac(String telefoneSac) {
    this.telefoneSac = telefoneSac;
  }

  public Double getPreco() {
    return preco;
  }

  public void setPreco(Double preco) {
    this.preco = preco;
  }

  public Integer getQuantidade() {
    return quantidade;
  }

  public void setQuantidade(Integer quantidade) {
    this.quantidade = quantidade;
  }

  public List<ReacoesAdversasModel> getReacoesAdversas() {
    return reacoesAdversas;
  }

  public void setReacoesAdversas(List<ReacoesAdversasModel> reacoesAdversas) {
    this.reacoesAdversas = reacoesAdversas;
  }

  public void addReacoesAdversas(ReacoesAdversasModel reacaoAdversa) {
    this.reacoesAdversas.add(reacaoAdversa);
  }

  public FabricanteModel getFabricanteId() {
    return fabricanteId;
  }

  public void setFabricanteId(FabricanteModel fabricanteId) {
    this.fabricanteId = fabricanteId;
  }
}
