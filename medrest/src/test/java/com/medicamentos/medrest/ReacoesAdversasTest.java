package com.medicamentos.medrest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.medicamentos.medrest.controllers.ReacoesAdversasController;
import com.medicamentos.medrest.dtos.ReacoesAdversasDTO;
import com.medicamentos.medrest.models.ReacoesAdversasModel;
import com.medicamentos.medrest.services.ReacoesAdversasService;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@WebMvcTest(ReacoesAdversasController.class)
public class ReacoesAdversasTest {

  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private WebApplicationContext webApplicationContext;

  private ObjectMapper objectMapper;

  @MockBean
  private ReacoesAdversasService reacoesAdversasService;

  private ReacoesAdversasDTO reacoesAdversasDTO;

  private ReacoesAdversasModel reacoesAdversasModel;


  private UUID id;

  public ReacoesAdversasTest() {
  }

  @BeforeEach
  public void setup() {
    MockitoAnnotations.openMocks(this);
    mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

    objectMapper = new ObjectMapper();

    id = UUID.randomUUID();

    reacoesAdversasDTO = new ReacoesAdversasDTO();
    reacoesAdversasDTO.setDescricao("Dor de cabeça");

    reacoesAdversasModel = new ReacoesAdversasModel();
    reacoesAdversasModel.setId(id);
    reacoesAdversasModel.setDescricao("Dor de cabeça");

  }

  @Test
  public void testSalvarReacaoAdversa() throws Exception {
    when(reacoesAdversasService.save(id, reacoesAdversasDTO)).thenReturn(
        reacoesAdversasModel);

    mockMvc.perform(post("/reacoes-adversas/medicamento/" + id).contentType(
                MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(reacoesAdversasDTO)))
        .andExpect(status().isCreated());
  }

  @Test
  public void testListarReacaoAdversa() throws Exception {
    Page<ReacoesAdversasModel> page = new PageImpl<>(
        Collections.singletonList(reacoesAdversasModel), PageRequest.of(0, 10),
        1);

    when(reacoesAdversasService.findAll(any(Pageable.class))).thenReturn(page);

    mockMvc.perform(get("/reacoes-adversas")).andExpect(status().isOk());
  }

  @Test
  public void listarReacao_quandoReacaoNaoExiste_deveRetornarStatusNotFound()
      throws Exception {
    UUID id = UUID.randomUUID();

    when(reacoesAdversasService.findById(id)).thenReturn(Optional.empty());

    mockMvc.perform(get("/reacoes-adversas/" + id))
        .andExpect(status().isNotFound());
  }

  @Test
  public void deletarReacao_deveRetornarStatusOk() throws Exception {
    UUID id = UUID.randomUUID();
    ReacoesAdversasModel reacao = new ReacoesAdversasModel();
    reacao.setId(id);

    when(reacoesAdversasService.findById(id)).thenReturn(Optional.of(reacao));

    mockMvc.perform(delete("/reacoes-adversas/" + id))
        .andExpect(status().isOk())
        .andExpect(content().string("Reação Adversa deletada com sucesso!"));
  }

  @Test
  public void deletarReacao_quandoReacaoNaoExiste_deveRetornarStatusNotFound()
      throws Exception {
    UUID id = UUID.randomUUID();

    when(reacoesAdversasService.findById(id)).thenReturn(Optional.empty());

    mockMvc.perform(delete("/reacoes-adversas" + id))
        .andExpect(status().isNotFound());
  }

  @Test
  public void atualizarReacao_deveRetornarStatusOk() throws Exception {
    UUID id = UUID.randomUUID();
    ReacoesAdversasDTO reacaoAdversaDTO = new ReacoesAdversasDTO();
    reacaoAdversaDTO.setDescricao("Febre");

    when(reacoesAdversasService.findById(id)).thenReturn(
        Optional.of(new ReacoesAdversasModel()));

    mockMvc.perform(
            put("/reacoes-adversas/" + id).contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(reacaoAdversaDTO)))
        .andExpect((status().isOk()));
  }

  @Test
  public void atualizarReacao_quandoReacaoNaoExiste_deveRetornarStatusNotFound()
      throws Exception {
    UUID id = UUID.randomUUID();

    when(reacoesAdversasService.findById(id)).thenReturn(Optional.empty());

    mockMvc.perform(
            put("/reacoes-adversas/" + id).contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(reacoesAdversasDTO)))
        .andExpect(status().isNotFound())
        .andExpect(content().string("Reação Adversa não encontrada"));
  }
}
