package com.medicamentos.medrest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.medicamentos.medrest.controllers.FabricanteController;
import com.medicamentos.medrest.dtos.FabricanteDTO;
import com.medicamentos.medrest.models.FabricanteModel;
import com.medicamentos.medrest.services.FabricanteService;
import java.util.Collections;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@WebMvcTest(FabricanteController.class)
public class FabricanteTest {

  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private WebApplicationContext webApplicationContext;

  @Autowired
  private ObjectMapper objectMapper;

  @MockBean
  private FabricanteService fabricanteService;

  private FabricanteDTO fabricanteDTO;
  private FabricanteModel fabricanteModel;


  private UUID id;

  public FabricanteTest() {
  }

  @BeforeEach
  void setup() {
    MockitoAnnotations.openMocks(this);
    mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

    objectMapper = new ObjectMapper();

    id = UUID.randomUUID();

    fabricanteDTO = new FabricanteDTO(id, "Fabricante 1");

    fabricanteModel = new FabricanteModel();
    fabricanteModel.setId(id);
    fabricanteModel.setNome("Fabricante 1");
  }

  @Test
  public void listarFabricantes_deveRetornarUmaListaPaginadaDeFabricantes()
      throws Exception {
    Page<FabricanteModel> page = new PageImpl<>(
        Collections.singletonList(fabricanteModel), PageRequest.of(0, 10), 1);

    when(fabricanteService.findAll(any(Pageable.class))).thenReturn(page);

    mockMvc.perform(get("/fabricantes")).andExpect(status().isOk());
  }

  @Test
  public void salvarFabricante_deveSalvarUmNovoFabricante() throws Exception {
    when(fabricanteService.save(ArgumentMatchers.any())).thenReturn(
        fabricanteModel);

    mockMvc.perform(MockMvcRequestBuilders.post("/fabricantes")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(objectMapper.writeValueAsString(fabricanteDTO)))
        .andExpect(MockMvcResultMatchers.status().isCreated()).andExpect(
            MockMvcResultMatchers.jsonPath("$.nome")
                .value(fabricanteDTO.getNome()));
  }
}
