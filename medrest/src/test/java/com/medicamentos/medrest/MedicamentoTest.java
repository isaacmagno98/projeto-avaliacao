package com.medicamentos.medrest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.medicamentos.medrest.controllers.MedicamentoController;
import com.medicamentos.medrest.dtos.MedicamentoDTO;
import com.medicamentos.medrest.models.MedicamentoModel;
import com.medicamentos.medrest.services.MedicamentoService;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@WebMvcTest(MedicamentoController.class)
public class MedicamentoTest {

  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private WebApplicationContext webApplicationContext;

  private ObjectMapper objectMapper;
  @MockBean
  private MedicamentoService medicamentoService;


  private MedicamentoDTO medicamentoDTO;

  private MedicamentoModel medicamentoModel;

  private UUID id;

  public MedicamentoTest() {
  }

  @BeforeEach
  public void setup() {
    MockitoAnnotations.openMocks(this);
    mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

    objectMapper = new ObjectMapper();

    id = UUID.randomUUID();

    medicamentoDTO = new MedicamentoDTO();
    medicamentoDTO.setNome("Dorflex");
    medicamentoDTO.setPreco(10.0);

    medicamentoModel = new MedicamentoModel();
    medicamentoModel.setId(id);
    medicamentoModel.setNome("Dorflex");
    medicamentoModel.setPreco(10.0);
  }

  @Test
  public void testSalvarMedicamento() throws Exception {
    when(medicamentoService.save(any(MedicamentoDTO.class))).thenReturn(
        medicamentoModel);

    mockMvc.perform(
            post("/medicamentos").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(medicamentoDTO)))
        .andExpect(status().isCreated());
  }

  @Test
  public void testListarMedicamentos() throws Exception {
    Page<MedicamentoModel> page = new PageImpl<>(
        Collections.singletonList(medicamentoModel), PageRequest.of(0, 10), 1);

    when(medicamentoService.findAll(any(Pageable.class))).thenReturn(page);

    mockMvc.perform(get("/medicamentos")).andExpect(status().isOk());
  }

  @Test
  public void listarMedicamento_quandoMedicamentoNaoExiste_deveRetornarStatusNotFound()
      throws Exception {
    UUID id = UUID.randomUUID();

    when(medicamentoService.findById(id)).thenReturn(Optional.empty());

    mockMvc.perform(get("/medicamentos/" + id))
        .andExpect(status().isNotFound());
  }

  @Test
  public void deletarMedicamento_deveRetornarStatusOk() throws Exception {
    UUID id = UUID.randomUUID();
    MedicamentoModel medicamento = new MedicamentoModel();
    medicamento.setId(id);

    when(medicamentoService.findById(id)).thenReturn(Optional.of(medicamento));

    mockMvc.perform(delete("/medicamentos/" + id)).andExpect(status().isOk())
        .andExpect(content().string("Medicamento deletado com sucesso!"));
  }

  @Test
  public void deletarMedicamento_quandoMedicamentoNaoExiste_deveRetornarStatusNotFound()
      throws Exception {
    UUID id = UUID.randomUUID();

    when(medicamentoService.findById(id)).thenReturn(Optional.empty());

    mockMvc.perform(delete("/medicamentos/" + id))
        .andExpect(status().isNotFound());
  }

  @Test
  public void atualizarMedicamento_deveRetornarStatusOk() throws Exception {
    UUID id = UUID.randomUUID();
    MedicamentoDTO medicamentoDTO = new MedicamentoDTO();
    medicamentoDTO.setNome("Paracetamol");

    when(medicamentoService.findById(id)).thenReturn(
        Optional.of(new MedicamentoModel()));

    mockMvc.perform(
            put("/medicamentos/" + id).contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(medicamentoDTO)))
        .andExpect(status().isOk());
  }

  @Test
  void atualizarMedicamento_quandoMedicamentoNaoExiste_deveRetornarStatusNotFound()
      throws Exception {
    UUID id = UUID.randomUUID();

    when(medicamentoService.findById(id)).thenReturn(Optional.empty());

    mockMvc.perform(
            put("/medicamentos/"+ id).contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(medicamentoDTO)))
        .andExpect(status().isNotFound())
        .andExpect(content().string("Medicamento não encontrado!"));
  }
}