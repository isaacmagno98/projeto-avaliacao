import { useState } from "react";

function Formulario() {
  const [formData, setFormData] = useState({
    registroAnvisa: "",
    nomeMedicamento: "",
    dataValidade: "",
    telefoneSAC: "",
    preco: "",
    quantidadeComprimidos: "",
    fabricante: "",
  });

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    console.log(formData);
  };

  return (
    <div className='max-w-lg mx-auto'>
      <h2 className='text-2xl font-semibold mb-4'>Cadastro de Medicamento</h2>
      <form onSubmit={handleSubmit} className='space-y-4'>
        <div>
          <label
            htmlFor='registroAnvisa'
            className='block text-gray-700 font-medium'
          >
            Número de Registro da Anvisa
          </label>
          <input
            type='text'
            id='registroAnvisa'
            name='registroAnvisa'
            value={formData.registroAnvisa}
            onChange={handleInputChange}
            className='border-gray-300 border rounded-md px-4 py-2 w-full focus:outline-none focus:ring focus:ring-blue-200'
            placeholder='0.0000.0000.000-0'
            maxLength='15'
            pattern='\d{1}\.\d{4}\.\d{4}\.\d{3}-\d{1}'
            required
          />
        </div>
        <div>
          <label
            htmlFor='nomeMedicamento'
            className='block text-gray-700 font-medium'
          >
            Nome do Medicamento
          </label>
          <input
            type='text'
            id='nomeMedicamento'
            name='nomeMedicamento'
            value={formData.nomeMedicamento}
            onChange={handleInputChange}
            className='border-gray-300 border rounded-md px-4 py-2 w-full focus:outline-none focus:ring focus:ring-blue-200'
            required
          />
        </div>
        <div>
          <label
            htmlFor='dataValidade'
            className='block text-gray-700 font-medium'
          >
            Data de Validade
          </label>
          <input
            type='text'
            id='dataValidade'
            name='dataValidade'
            value={formData.dataValidade}
            onChange={handleInputChange}
            className='border-gray-300 border rounded-md px-4 py-2 w-full focus:outline-none focus:ring focus:ring-blue-200'
            placeholder='dd/mm/aaaa'
            maxLength='10'
            pattern='\d{2}\/\d{2}\/\d{4}'
            required
          />
        </div>
        <div>
          <label
            htmlFor='telefoneSAC'
            className='block text-gray-700 font-medium'
          >
            Telefone do SAC
          </label>
          <input
            type='tel'
            id='telefoneSAC'
            name='telefoneSAC'
            value={formData.telefoneSAC}
            onChange={handleInputChange}
            className='border-gray-300 border rounded-md px-4 py-2 w-full focus:outline-none focus:ring focus:ring-blue-200'
            placeholder='(00)0000-0000'
            maxLength='14'
            pattern='\([0-9]{2}\)[0-9]{4,5}-[0-9]{4}'
            required
          />
        </div>
        <div>
          <label htmlFor='preco' className='block text-gray-700 font-medium'>
            Preço em Reais
          </label>
          <input
            type='text'
            id='preco'
            name='preco'
            value={formData.preco}
            onChange={handleInputChange}
            className='border-gray-300 border rounded-md px-4 py-2 w-full focus:outline-none focus:ring focus:ring-blue-200'
            placeholder='0,00'
            maxLength='6'
            pattern='([0-9]{1,3}\.)?[0-9]{1,3},[0-9]{2}'
            required
          />
        </div>
        <div>
          <label
            htmlFor='quantidadeComprimidos'
            className='block text-gray-700 font-medium'
          >
            Quantidade de Comprimidos
          </label>
          <input
            type='number'
            id='quantidadeComprimidos'
            name='quantidadeComprimidos'
            value={formData.quantidadeComprimidos}
            onChange={handleInputChange}
            className='border-gray-300 border rounded-md px-4 py-2 w-full focus:outline-none focus:ring focus:ring-blue-200'
            required
          />
        </div>
        <div>
          <label
            htmlFor='fabricante'
            className='block text-gray-700 font-medium'
          >
            Fabricante
          </label>
          <input
            type='text'
            id='fabricante'
            name='fabricante'
            value={formData.fabricante}
            onChange={handleInputChange}
            className='border-gray-300 border rounded-md px-4 py-2 w-full focus:outline-none focus:ring focus:ring-blue-200'
            required
          />
        </div>
        <button
          type='submit'
          className='bg-blue-500 text-white py-2 px-4 rounded'
        >
          Enviar
        </button>
      </form>
    </div>
  );
}

export default Formulario;
