import { createSlice } from "@reduxjs/toolkit";

export const medicamentosSlice = createSlice({
  name: "Medicamentos",
  initialState: {
    medicamentos: [],
  },
  reducers: {
    setMedicamentos(state, { payload }) {
      return { ...state, medicamentos: payload };
    },
  },
});

export const { setMedicamentos } = medicamentosSlice.actions;

export default medicamentosSlice.reducer;
