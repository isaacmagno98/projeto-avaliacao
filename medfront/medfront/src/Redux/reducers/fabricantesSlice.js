import { createSlice } from "@reduxjs/toolkit";

export const fabricantesSlice = createSlice({
  name: "Fabricantes",
  initialState: {
    fabricantes: [],
  },
  reducers: {
    setFabricantes(state, { payload }) {
      return { ...state, fabricantes: payload };
    },
  },
});

export const { setFabricantes } = fabricantesSlice.actions;

export default fabricantesSlice.reducer;
