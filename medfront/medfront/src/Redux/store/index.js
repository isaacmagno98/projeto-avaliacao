import { configureStore } from "@reduxjs/toolkit";
import MedicamentosSlice from "../reducers/medicamentosSlice";
import FabricantesSlice from "../reducers/fabricantesSlice";

export default configureStore({
  reducer: {
    medicamentos: MedicamentosSlice,
    fabricantes: FabricantesSlice,
  },
});
