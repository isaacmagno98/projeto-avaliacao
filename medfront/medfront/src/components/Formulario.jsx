import React, { useEffect, useState } from "react";
import Select from "react-select";
import { useSelector, useDispatch } from "react-redux";
import { GET_MEDICAMENTOS, POST_MEDICAMENTO } from "../services/axiosRequests";

import InputMask from "react-input-mask";
import { currencyMask } from "../helpers/currencyMask";
import { setMedicamentos } from "../Redux/reducers/medicamentosSlice";
import { converterData } from "../helpers/dataConverter";

const Formulario = () => {
  const [registroAnvisa, setRegistroAnvisa] = useState();
  const [nomeMedicamento, setNomeMedicamento] = useState();
  const [dataValidade, setDataValidade] = useState();
  const [telefoneSAC, setTelefoneSAC] = useState();
  const [preco, setPreco] = useState();
  const [quantidadeComprimidos, setQuantidadeComprimidos] = useState();
  const [fabricante, setFabricante] = useState();
  const [fabricantesSelector, setFabricantesSelector] = useState([]);

  const { fabricantes } = useSelector((state) => state.fabricantes);

  const dispatch = useDispatch();

  useEffect(() => {
    const fabricantesFormated = [];

    if (fabricantes.length) {
      for (let i = 0; i < fabricantes.length; i++) {
        fabricantesFormated.push({
          value: fabricantes[i].id,
          label: fabricantes[i].nome,
        });
      }
    }

    setFabricantesSelector(fabricantesFormated);
  }, [fabricantes]);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const dataValidadeFormatada = converterData(dataValidade);

    await POST_MEDICAMENTO({
      anvisa: registroAnvisa,
      nome: nomeMedicamento,
      dataValidade: dataValidadeFormatada,
      telefoneSac: telefoneSAC,
      preco,
      quantidade: quantidadeComprimidos,
      fabricanteId: fabricante,
    });

    await GET_MEDICAMENTOS().then((data) => dispatch(setMedicamentos(data)));

    setRegistroAnvisa("");
    setNomeMedicamento("");
    setDataValidade("");
    setTelefoneSAC("");
    setPreco("");
    setQuantidadeComprimidos("");
    setFabricante("");
  };

  return (
    <div className='flex justify-center items-center h-screen bg-green-200'>
      <div className='max-w-lg mx-auto bg-blue-300 p-6 rounded shadow-md'>
        <h2 className='text-2xl font-semibold mb-8 text-center'>
          Cadastro de Medicamento
        </h2>
        <form onSubmit={handleSubmit} className='grid grid-cols-2 gap-4 mb-6'>
          <div>
            <label
              htmlFor='registroAnvisa'
              className='block text-gray-700 font-medium'
            >
              Número de Registro da Anvisa
            </label>
            <InputMask
              required
              mask='9.9999.9999.999-9'
              placeholder='0.0000.0000.000-0'
              value={registroAnvisa}
              onChange={(e) => setRegistroAnvisa(e.target.value)}
              className='border-gray-300 border rounded-md px-4 py-2 w-full focus:outline-none focus:ring focus:ring-blue-200'
            />
          </div>
          <div>
            <label
              htmlFor='nomeMedicamento'
              className='block text-gray-700 font-medium'
            >
              Nome do Medicamento
            </label>
            <input
              type='text'
              id='nomeMedicamento'
              name='nomeMedicamento'
              placeholder='Ex: Dipirona'
              value={nomeMedicamento}
              onChange={(e) => setNomeMedicamento(e.target.value)}
              className='border-gray-300 border rounded-md px-4 py-2 w-full focus:outline-none focus:ring focus:ring-blue-200'
              required
            />
          </div>
          <div>
            <label
              htmlFor='dataValidade'
              className='block text-gray-700 font-medium'
            >
              Data de Validade
            </label>
            <InputMask
              required
              mask='99/99/9999'
              value={dataValidade}
              placeholder='dd/mm/aaaa'
              onChange={(e) => setDataValidade(e.target.value)}
              className='border-gray-300 border rounded-md px-4 py-2 w-full focus:outline-none focus:ring focus:ring-blue-200'
            />
          </div>
          <div>
            <label
              htmlFor='telefoneSAC'
              className='block text-gray-700 font-medium'
            >
              Telefone do SAC
            </label>
            <InputMask
              required
              mask='(99)9999-9999'
              placeholder='(00)0000-0000'
              value={telefoneSAC}
              onChange={(e) => setTelefoneSAC(e.target.value)}
              className='border-gray-300 border rounded-md px-4 py-2 w-full focus:outline-none focus:ring focus:ring-blue-200'
            />
          </div>
          <div>
            <label htmlFor='preco' className='block text-gray-700 font-medium'>
              Preço em Reais
            </label>
            <input
              required
              type='text'
              placeholder='0,00'
              value={preco}
              onChange={(e) => setPreco(currencyMask(e))}
              className='border-gray-300 border rounded-md px-4 py-2 w-full focus:outline-none focus:ring focus:ring-blue-200'
            />
          </div>
          <div>
            <label
              htmlFor='quantidadeComprimidos'
              className='block text-gray-700 font-medium'
            >
              Quantidade de Comprimidos
            </label>
            <input
              type='number'
              id='quantidadeComprimidos'
              name='quantidadeComprimidos'
              placeholder='0'
              value={quantidadeComprimidos}
              onChange={(e) => setQuantidadeComprimidos(e.target.value)}
              className='border-gray-300 border rounded-md px-4 py-2 w-full focus:outline-none focus:ring focus:ring-blue-200'
              required
            />
          </div>
          <div>
            <label
              htmlFor='fabricante'
              className='block text-gray-700 font-medium'
            >
              Fabricante
            </label>
            <Select
              required
              options={fabricantesSelector}
              onChange={(e) => setFabricante(e.value)}
              className='border-gray-300 border rounded-md w-full focus:outline-none focus:ring focus:ring-blue-200'
            />
          </div>
          <button
            type='submit'
            className='bg-green-800 text-white py-2 px-4 rounded '
          >
            Enviar
          </button>
        </form>
      </div>
    </div>
  );
};

export default Formulario;
