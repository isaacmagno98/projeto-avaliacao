import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import DataTable from "react-data-table-component";
import { useNavigate } from "react-router-dom";

const Medicamentos = () => {
  const [listaDeMedicamentos, setListaDeMedicamentos] = useState();
  const [columns, setColumns] = useState();
  const [data, setData] = useState();
  const [searchTerm, setSearchTerm] = useState("");

  const navigate = useNavigate();

  const { medicamentos } = useSelector((state) => state.medicamentos);

  if (data) {
    var filteredData = data.filter(
      (item) =>
        item.medicamento.toLowerCase().includes(searchTerm.toLowerCase()) ||
        item.registro.toLowerCase().includes(searchTerm.toLowerCase())
    );
  }

  useEffect(() => {
    if (medicamentos) setListaDeMedicamentos(medicamentos);
  }, [medicamentos]);

  useEffect(() => {
    setColumns([
      {
        id: "registro",
        name: "Registro",
        selector: (row) => row.registro,
        sortable: true,
      },
      {
        id: "medicamento",
        name: "Medicamento",
        selector: (row) => row.medicamento,
        sortable: true,
      },
      {
        id: "anvisa",
        name: "N° Anvisa",
        selector: (row) => row.anvisa,
      },
      {
        id: "dataValidade",
        name: "Data Validade",
        selector: (row) => row.dataValidade,
      },
      {
        id: "telefoneSac",
        name: "Telefone SAC",
        selector: (row) => row.telefoneSac,
      },
      {
        id: "preco",
        name: "Preço",
        selector: (row) => row.preco,
      },
      {
        id: "quantidade",
        name: "Quantidade",
        selector: (row) => row.quantidade,
      },
      {
        id: "fabricante",
        name: "Fabricante",
        selector: (row) => row.fabricante.nome,
      },
    ]);

    if (listaDeMedicamentos) {
      setData(
        listaDeMedicamentos.map((med) => ({
          registro: med.id,
          medicamento: med.nome,
          anvisa: med.anvisa,
          dataValidade: med.dataValidade,
          telefoneSac: med.telefoneSac,
          preco: med.preco,
          quantidade: med.quantidade,
          fabricante: med.fabricanteId,
          reacoesAdversas: med.reacoesAdversas,
        }))
      );
    }
  }, [listaDeMedicamentos]);

  return (
    <div className='flex justify-center items-center'>
      <div className='mx-auto my-auto'>
        <input
          type='text'
          placeholder='Filtrar...'
          onChange={(event) => setSearchTerm(event.target.value)}
          className='border-white border  px-4 py-2 focus:outline-none my-2 mt-12 rounded w-96'
        />
        <DataTable
          pagination
          responsive
          highlightOnHover
          defaultSortFieldId='medicamento'
          theme='light'
          columns={columns}
          data={filteredData ? filteredData : data}
          onFilter={(value) => setSearchTerm(value)}
          onRowClicked={(e) =>
            navigate("/detalhes", { state: { medicamento: e } })
          }
        />
      </div>
    </div>
  );
};

export default Medicamentos;
