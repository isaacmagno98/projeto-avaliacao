import axios from "axios";

export const BASE_URL = "http://localhost:8080";

export const GET_MEDICAMENTOS = async () => {
  const medicamentos = await axios
    .get(`${BASE_URL}/medicamentos`)
    .then((object) => object.data.content);

  return medicamentos;
};

export const POST_MEDICAMENTO = async (data) => {
  const {
    anvisa,
    nome,
    dataValidade,
    telefoneSac,
    preco,
    quantidade,
    fabricanteId,
  } = data;

  return await axios
    .post(`${BASE_URL}/medicamentos`, {
      nome,
      anvisa,
      dataValidade,
      telefoneSac,
      preco,
      quantidade,
      fabricanteId,
    })
    .then((response) => response.data);
};

export const PUT_MEDICAMENTO = async (data, id) => {
  console.log(data);

  const {
    anvisa,
    nome,
    dataValidade,
    telefoneSac,
    preco,
    quantidade,
    fabricanteId,
  } = data;

  return await axios
    .put(`${BASE_URL}/medicamentos/${id}`, {
      nome,
      anvisa,
      dataValidade,
      telefoneSac,
      preco,
      quantidade,
      fabricanteId,
    })
    .then((response) => response.data);
};

export const DELETE_MEDICAMENTO = async (id) => {
  return await axios.delete(`${BASE_URL}/medicamentos/${id}`);
};

export const PUT_REACAO_ADVERSA = async (data) => {
  const { descricao, id, medicamentoID } = data;

  return await axios.put(`${BASE_URL}/reacoes-adversas/${id}`, {
    descricao,
    medicamentoID,
  });
};

export const DELETE_REACAO_ADVERSA = async (id) => {
  return await axios.delete(`${BASE_URL}/reacoes-adversas/${id}`);
};

export const POST_REACAO_ADVERSA = async (data) => {
  const { descricao, id } = data;

  return await axios.post(`${BASE_URL}/reacoes-adversas/medicamento/${id}`, {
    descricao,
  });
};

export const GET_FABRICANTES = async () => {
  const medicamentos = await axios
    .get(`${BASE_URL}/fabricantes`)
    .then((object) => object.data.content);

  return medicamentos;
};
