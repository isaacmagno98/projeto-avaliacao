import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { useLocation, useNavigate } from "react-router-dom";

import { setMedicamentos } from "../Redux/reducers/medicamentosSlice";

import {
  DELETE_REACAO_ADVERSA,
  GET_MEDICAMENTOS,
  PUT_REACAO_ADVERSA,
  POST_REACAO_ADVERSA,
  PUT_MEDICAMENTO,
  DELETE_MEDICAMENTO,
} from "../services/axiosRequests";

const Details = () => {
  const [editar, setEditar] = useState(false);
  const [conteudoEditado, setConteudoEditado] = useState();
  const [reacaoAdversa, setReacaoAdversa] = useState();
  const [listaReacoes, setListaReacoes] = useState([]);

  const [nomeMedicamento, setNomeMedicamento] = useState();
  const [precoMedicamento, setPrecoMedicamento] = useState();
  const [quantidadeMedicamento, setQuantidadeMedicamento] = useState();
  const [telefoneSac, setTelefoneSac] = useState();

  const [editNomeMedicamento, setEditNomeMedicamento] = useState();
  const [editPrecoMedicamento, setEditPrecoMedicamento] = useState();
  const [editQuantidadeMedicamento, setEditQuantidadeMedicamento] = useState();
  const [editTelefoneSac, setEditTelefoneSac] = useState();

  const dispatch = useDispatch();

  const location = useLocation();
  const medicamento = location.state.medicamento;

  const navigate = useNavigate();

  useEffect(() => {
    setListaReacoes(medicamento.reacoesAdversas);
    setNomeMedicamento(medicamento.medicamento);
    setPrecoMedicamento(medicamento.preco);
    setQuantidadeMedicamento(medicamento.quantidade);
    setTelefoneSac(medicamento.telefoneSac);
  }, [medicamento]);

  console.log(medicamento);

  useEffect(() => {}, [
    listaReacoes,
    nomeMedicamento,
    precoMedicamento,
    quantidadeMedicamento,
    telefoneSac,
  ]);

  const updateDb = async () => {
    await GET_MEDICAMENTOS().then((data) => dispatch(setMedicamentos(data)));
  };

  const handleUpdate = async (tipo, id) => {
    if (tipo === "reacaoAdversa") {
      if (conteudoEditado.length) {
        await PUT_REACAO_ADVERSA({
          descricao: conteudoEditado,
          id,
          medicamentoID: medicamento.registro,
        });

        const reacaoEditada = listaReacoes.filter((reacao) => reacao.id === id);
        reacaoEditada.descricao = conteudoEditado;

        setListaReacoes([
          ...listaReacoes.filter((reacao) => reacao.id !== id),
          reacaoEditada,
        ]);

        conteudoEditado("");
      }
    }

    if (tipo === "medicamento") {
      if (
        editNomeMedicamento ||
        editPrecoMedicamento ||
        editQuantidadeMedicamento ||
        editTelefoneSac
      ) {
        await PUT_MEDICAMENTO(
          {
            nome: editNomeMedicamento || medicamento.medicamento,
            anvisa: medicamento.anvisa,
            dataValidade: medicamento.dataValidade,
            telefoneSac: editTelefoneSac || medicamento.telefoneSac,
            preco: editPrecoMedicamento || medicamento.preco,
            quantidade: editQuantidadeMedicamento || medicamento.quantidade,
            fabricanteId: medicamento.fabricante.id,
          },
          medicamento.registro
        );

        if (editNomeMedicamento) setNomeMedicamento(editNomeMedicamento);
        if (editPrecoMedicamento) setPrecoMedicamento(editPrecoMedicamento);
        if (editQuantidadeMedicamento)
          setQuantidadeMedicamento(editQuantidadeMedicamento);
        if (editTelefoneSac) setTelefoneSac(editTelefoneSac);

        setEditNomeMedicamento("");
        setEditPrecoMedicamento("");
        setEditQuantidadeMedicamento("");
        setEditTelefoneSac("");
      }
    }

    updateDb();
  };

  const handleDelete = async (id) => {
    await DELETE_REACAO_ADVERSA(id);

    setListaReacoes([...listaReacoes.filter((reacao) => reacao.id !== id)]);

    updateDb();
  };

  const handleSubmit = async () => {
    await POST_REACAO_ADVERSA({
      descricao: reacaoAdversa,
      id: medicamento.registro,
    });

    setListaReacoes([
      ...listaReacoes,
      {
        descricao: reacaoAdversa,
        id: medicamento.registro,
      },
    ]);

    updateDb();

    setReacaoAdversa("");
  };

  const handleDeleteMedicamento = async () => {
    await DELETE_MEDICAMENTO(medicamento.registro);

    updateDb();
    navigate("/");
  };

  const returnHome = () => {
    updateDb();
    navigate("/");
  };

  return (
    <div className='bg-blue-300 flex min-h-screen justify-center items-center'>
      <div className='p-12 bg-white/80 rounded shadow-md'>
        <div className='flex justify-between gap-8'>
          <p className='font-bold'>Registro: </p>
          <p>{medicamento.registro}</p>
        </div>
        <div className='flex justify-between gap-8'>
          <p className='font-bold'>Nome: </p>
          {editar ? (
            <div
              contentEditable={true}
              onInput={(e) => setEditNomeMedicamento(e.target.innerHTML)}
              onBlur={() => handleUpdate("medicamento")}
              className='bg-green-200 px-2 rounded'
            >
              {nomeMedicamento}
            </div>
          ) : (
            <p>{nomeMedicamento}</p>
          )}
        </div>
        <div className='flex justify-between gap-8'>
          <p className='font-bold'>N° Registro Anvisa: </p>
          <p>{medicamento.anvisa}</p>
        </div>
        <div className='flex justify-between gap-8'>
          <p className='font-bold'>Data de Validade: </p>
          <p>{medicamento.dataValidade}</p>
        </div>
        <div className='flex justify-between gap-8'>
          <p className='font-bold'>Fabricante: </p>
          <p>{medicamento.fabricante.nome}</p>
        </div>
        <div className='flex justify-between gap-8'>
          <p className='font-bold'>Preço: </p>
          {editar ? (
            <div
              contentEditable={true}
              onInput={(e) => setEditPrecoMedicamento(e.target.innerHTML)}
              onBlur={() => handleUpdate("medicamento")}
              className='bg-green-200 px-2 rounded'
            >
              {precoMedicamento}
            </div>
          ) : (
            <p>{precoMedicamento}</p>
          )}
        </div>
        <div className='flex justify-between gap-8'>
          <p className='font-bold'>Quantidade:</p>
          {editar ? (
            <div
              contentEditable={true}
              onInput={(e) => setEditQuantidadeMedicamento(e.target.innerHTML)}
              onBlur={() => handleUpdate("medicamento")}
              className='bg-green-200 px-2 rounded'
            >
              {quantidadeMedicamento}
            </div>
          ) : (
            <p> {quantidadeMedicamento}</p>
          )}
        </div>
        <div className='flex justify-between gap-8'>
          <p className='font-bold'>TelefoneSAC:</p>
          {editar ? (
            <div
              contentEditable={true}
              onInput={(e) => setEditTelefoneSac(e.target.innerHTML)}
              onBlur={() => handleUpdate("medicamento")}
              className='bg-green-200 px-2 rounded'
            >
              {telefoneSac}
            </div>
          ) : (
            <p>{telefoneSac}</p>
          )}
        </div>
        <div className=''>
          <p className='font-bold mb-2'>Reações Adversas:</p>
          <div className=''>
            {listaReacoes.map((reacao) => (
              <div className=''>
                {editar ? null : (
                  <p className='mr-2 font-light'>{reacao.descricao} </p>
                )}
                {editar ? (
                  <div className='flex justify-between'>
                    <div
                      contentEditable={true}
                      onInput={(e) => setConteudoEditado(e.target.innerHTML)}
                      onBlur={() => handleUpdate("reacaoAdversa", reacao.id)}
                      className='bg-green-200 px-2 rounded'
                    >
                      {reacao.descricao}
                    </div>
                    <button
                      className='text-white bg-red-500 px-2 font-bold rounded mt-0.5'
                      onClick={() => handleDelete(reacao.id)}
                    >
                      x
                    </button>
                  </div>
                ) : null}
              </div>
            ))}
          </div>
          {editar ? (
            <div className='flex justify-between mt-2'>
              <input
                type='text'
                className='bg-green-200 rounded-l flex-grow'
                value={reacaoAdversa}
                onChange={(e) => setReacaoAdversa(e.target.value)}
              />
              <button
                className='bg-green-600 p-2 rounded-r text-white font-bold'
                onClick={() => handleSubmit()}
              >
                Enviar
              </button>
            </div>
          ) : null}
        </div>
        <div className='flex justify-evenly mt-6 '>
          <button
            className='bg-blue-600 text-white py-3 px-6 rounded'
            type='button'
            onClick={() => setEditar(!editar)}
          >
            Editar
          </button>
          <button
            type='button'
            className='bg-red-700 text-white py-3 px-6 rounded '
            onClick={() => handleDeleteMedicamento()}
          >
            Excluir
          </button>
          <button
            type='button'
            className='bg-orange-700 text-white py-3 px-6 rounded '
            onClick={() => returnHome()}
          >
            Voltar
          </button>
        </div>
      </div>
    </div>
  );
};

export default Details;
