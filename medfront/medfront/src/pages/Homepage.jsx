import React from "react";
import FullPage, {
  FullPageSections,
  FullpageSection,
  FullpageNavigation,
} from "@ap.cx/react-fullpage";

import Formulario from "../components/Formulario";
import Medicamentos from "../components/Medicamentos";

const Homepage = () => {
  return (
    <FullPage>
      <FullpageNavigation />
      <FullPageSections className='bg-green-200'>
        <FullpageSection style={{ height: "100vh" }}>
          <Formulario />
        </FullpageSection>
        <FullpageSection style={{ height: "100vh" }}>
          <Medicamentos />
        </FullpageSection>
      </FullPageSections>
      <footer
        className='text-gray-300 flex items-end justify-center font-thin'
        id='footer'
      >
        ©2022 Isaac Magno. All rights reserved.
      </footer>
    </FullPage>
  );
};

export default Homepage;
