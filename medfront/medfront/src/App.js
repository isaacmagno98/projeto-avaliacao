import React, { useEffect } from "react";
import { Route, Routes } from "react-router-dom";

import Homepage from "./pages/Homepage";
import Details from "./pages/Details";

import { useDispatch } from "react-redux";
import { setMedicamentos } from "./Redux/reducers/medicamentosSlice";
import { setFabricantes } from "./Redux/reducers/fabricantesSlice";

import { GET_MEDICAMENTOS, GET_FABRICANTES } from "./services/axiosRequests";

const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    const getMedicamentos = async () =>
      await GET_MEDICAMENTOS().then((data) => dispatch(setMedicamentos(data)));

    const getFabricantes = async () =>
      await GET_FABRICANTES().then((data) => dispatch(setFabricantes(data)));

    getMedicamentos();
    getFabricantes();
  }, [dispatch]);

  return (
    <Routes>
      <Route path='/' element={<Homepage />} />
      <Route path='/detalhes' element={<Details />} />
    </Routes>
  );
};

export default App;
